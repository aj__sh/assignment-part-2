import csv
import sys

mat = dict()
bio = dict()
eng = dict()
phy = dict()
che = dict()
hin = dict()
total_percentage = dict()

def get_data(fname):
    with open(fname, mode='r') as fh:
        reader = csv.DictReader(fh)
        for row in reader:				
            name = row['Name']
            maths = int(row['Maths'])
            biology = int(row['Biology'])
            english =int(row['English'])
            physics = int(row['Physics'])
            chemistry = int(row['Chemistry'])
            hindi = int(row['Hindi'])

            mat[name] = maths
            bio[name] = biology
            eng[name] = english
            phy[name] = physics
            che[name] = chemistry
            hin[name] = hindi
            total_percentage[name] = (maths+biology+english+physics+chemistry+hindi)/6

def task():
    sub =[mat,bio,eng,phy,che,hin]
    subjects = ['Maths','Biology','English','Physics','Chemistry','Hindi']
    for i in range(6):
        topper = sorted(sub[i].items(), key = lambda kv:(kv[1], kv[0])).pop()
        print(f"Topper in {subjects[i]} is {topper[0]}")

    class_topper = sorted(total_percentage.items(), key = lambda kv:(kv[1], kv[0]))
    first = class_topper.pop()
    second = class_topper.pop()
    third  = class_topper.pop()
    print(f"Best Students in the class are {first[0]},{second[0]} and {third[0]}.")
    
if __name__ == '__main__':
    fname = sys.argv[1]
    get_data(fname)
    task()